
#include <stdbool.h>
#include "data.h"
#include <stdlib.h>
#include "mymalloc.h"
#include <stdio.h>
#include "list.h"


/************************************************************************/
/** \fn void ListInit(List_t* list)
 * \brief Provede inicializaci seznamu před jeho prvním pouitím.
 * \param list - seznam, který se má inicializovat
 *
 *	Inicializace spočívá v naplnění ukazatelů \link List_t#first list->first \endlink a
 *	\link List_t#active list->active \endlink nulovými ukazateli
 */
void List_Init(List_t * const list){

    list->active = NULL;
    list->first = NULL;

}

/************************************************************************/
/** \fn void InsertFirst(List_t* list, Data_t data)
 * \brief Vytvoří nový prvek a vloí jej na začátek seznamu.
 * \param list - seznam, do kterého se má nový prvek vkládat
 * \param data - data, která mají být obsahem nového prvního prvku
 *
 *	Vytvoří nový prvek s datovou částí "data" a vloí jej na začátek seznamu "list"
 */
void List_Insert_First(List_t* const list, Data_t data){
    List_Node_t * novy = myMalloc(sizeof(List_Node_t));

    if (list->first != NULL)
        novy->next = list->first;

    novy->data = data;
    list->first = novy;
}

/************************************************************************/
/** \fn void First(List_t* list)
 * \brief Nastaví ukazatel aktivního prvku na 1. prvek.
 * \param list - seznam, se kterým má být tato operace provedena
 *
 *	Nastaví \link List_t#active list->active \endlink na hodnotu
 *	\link List_t#first list->first \endlink
 */
void List_First(List_t* const list){
    list->active = list->first;
}

/************************************************************************/
/** \fn Data_t CopyFirst(List_t list)
 * \brief Vrátí data 1. prvku seznamu
 * \param list - seznam, se kterým má být tato operace provedena
 * \param *data - pointer, který slouží k navrácení data ze seznamu
 * \return Vrátí true, pokud je prvek přečten jinak vrátí false
 */
bool List_Copy_First(List_t list, Data_t *data){
    if (list.first == NULL) return false;

    *data = list.first->data;
    return true;
}

/************************************************************************/
/** \fn void DeleteFirst(List_t* list)
 * \brief Ruí první prvek seznamu.
 * \param list - seznam, se kterým má být tato operace provedena
 *
 *   Pokud byl první prvek zároveň aktivním prvkem, aktivita se ztrácí.
 *   Pokud byl seznam prázdný, nic se neděje.
 */
void List_Delete_First(List_t* const list){
    if (list->first != NULL){
        List_Node_t * del = list->first;
        // aktivni prvek - zruse se aktivita
        if(list->first == list->active)
            list->active = NULL;

        // zruseni prvniho prvku
        list->first = list->first->next;
        myFree(del);
    }
}

/************************************************************************/
/** \fn void PostDelete(List_t* list)
 * \brief Ruí první prvek seznamu za aktivním prvkem.
 * \param list - seznam, se kterým má být tato operace provedena
 *
 *   Pokud nebyl seznam aktivní, nic se neděje.
 *   Pokud byl seznam prázdný, nic se neděje.
 */
void List_Post_Delete(List_t* const list){
    if ((list->active != NULL) && (list->active->next != NULL)){
        // ukzatel na mazany prvek
        List_Node_t *tmp = list->active->next;

        // odebrani ukazatele na dalsi prvek
        list->active->next = tmp->next;

        //uvolneni
        myFree(tmp);
    }
}

/************************************************************************/
/** \fn void PostInsert(List_t* list, Data_t data)
 * \brief Vloí nový prvek za aktivní prvek seznamu.
 * \param list - seznam, do kterého se má nový prvek vkládat
 * \param data - data, která mají být obsahem nového prvku
 *
 *	Vytvoří nový prvek s datovou částí "data" a vloí jej za aktivní prvek
 *  seznamu "list". Pokud nebyl seznam aktivní, nedělá nic.
 */
void List_Post_Insert(List_t* const list, Data_t data){
    if(list->active != NULL){
        List_Node_t *newi = NULL;

        // alokace noveho prvku
        if((newi = myMalloc(sizeof(List_Node_t))) == NULL)
            fputs("List_Post_Insert: unable alocate memmory", stderr);

        else{
        newi->data = data;
        // posunut9
        newi->next = list->active->next;
        list->active->next = newi;
        }
    }
}


/************************************************************************/
/** \fn Data_t LCopy(List_t list)
 * \brief Vrátí data aktivního prvku seznamu
 * \param list - seznam, se kterým má být tato operace provedena
 * \param *data - pointer, který slouží k navrácení data ze seznamu
 * \return Vrátí true, pokud je prvek přečten jinak vrátí false
 *
 * Pokud seznam není aktivní vrátí false.
 */
bool List_Copy(List_t list, Data_t *data){
    if (list.active == NULL) return false;

    *data = list.active->data;
    return true;
}

/************************************************************************/
/** \fn void Actualize(List_t* list, Data_t data)
 * \brief Přepíe obsah aktivní poloky novými daty.
 * \param list - seznam, do kterého se má nový prvek vkládat
 * \param data - data, která mají být obsahem nového prvku
 *
 *  Pokud není seznam aktivní, nedělá nic.
 */
void List_Actualize(const List_t* const list, Data_t data){
    if (list->active != NULL){
        list->active->data = data;
    }
}


/************************************************************************/
/** \fn void LSucc(List_t* list)
 * \brief Posune aktivitu na následující prvek seznamu.
 * \param list - seznam, se kterým má být tato operace provedena
 *
 *  Pokud není ádný prvek aktivní, nedělá nic.
 */
void List_Succ(List_t* const list){

    if (list->active != NULL){
        list->active = list->active->next;
    }
}

/************************************************************************/
/** \fn bool Active(List_t list)
 * \brief Je-li seznam aktivní, vrací true
 * \param list - seznam, se kterým má být tato operace provedena
 */
bool List_Is_Active(List_t list){
    return (list.active != NULL ? true : false);
}


