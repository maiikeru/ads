#include "list.h"
#include "data.h"
#include "ioutils.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

void release_list(List_t  *list){
        while(list->first != NULL){
            List_Node_t * tmp = list->first;

            // posunut9 na dalsi prvek
            list->first = list->first->next;
            // uvoleni prvku
            myFree(tmp);
        }
        list->active = NULL;
}

void print_menu(){
    printf( "Zadejte pismeno 0-A pro jednu z nasledujicich cinnosti:\n"
                "0: Init,\n"
                "1: Actualize,\n"
                "2: Insert_First,\n"
                "3: First,\n"
                "4: Copy_First,\n"
                "5: Delete_First,\n"
                "6: Post_Delete,\n"
                "7: Post_Insert,\n"
                "8: Copy,\n"
                "9: Succ,,\n"
                "A: Is_Active,\n"
                "M: Vypis menu\n"
                "CTRL+Z (Win) nebo CTRL+D (Unix): Konec programu\n" );

}

void print_seznam(List_t  *list){
    printf("Aktivni polozka:\n");

    if(list->active != NULL){
        Data_Print(&list->active->data);
        printf("\n");
    }
    else
     printf("NULL\n");

    printf("Obsah seznamu:\n");

    List_Node_t *tmp  = list->first;
    int i = 1;
    while(tmp != NULL){
        printf("%d. prvek: ", i);
        Data_Print(&tmp->data);
        tmp = tmp->next;
        i++;
    }

    printf("\nZadejte znak 0-A, EOF(tj. CTRL+Z nebo CTRL+D)=Konec, M=Menu:\n"
            "************************************************************\n");
}

int main( int argc, char** argv ) {
    (void) argc;
    (void) argv;
    printf("List test program");

    bool run = true;
    List_t seznam;
    Data_t data;

    print_menu();
    while( run ) {
        char c;

        if( !io_utils_get_char( &c ) ) {
            break;
        }

        switch( c ) {
            case '0':
                printf("Vase volba=%c\n", c);
                printf("Init - inicializace seznamu\n");

                List_Init(&seznam);
                print_seznam(&seznam);

            break;

            case '1':
                printf("Vase volba=%c\n", c);
                printf("Actualize - prepis dat aktivni polozky\n");

                if (Data_Get(&data))
                    List_Actualize(&seznam, data);
                else
                    return EXIT_FAILURE;

                print_seznam(&seznam);
            break;

            case '2':
                printf("Vase volba=%c\n", c);
                printf("Insert_First - vlozeni nove polozky na 1. misto seznamu\n");

                if (Data_Get(&data))
                    List_Insert_First(&seznam, data);
                else
                    return EXIT_FAILURE;

                print_seznam(&seznam);

            break;

            case '3':
                printf("Vase volba=%c\n", c);
                printf("First - nastaveni aktivni polozky na 1.prvek");

                List_First(&seznam);
                print_seznam(&seznam);

            break;

            case '4':
                printf("Vase volba=%c\n", c);
                printf("Copy_First - Vypis 1.prvku seznamu\n");

                if( List_Copy_First(seznam, &data))
                    Data_Print(&data);

                print_seznam(&seznam);
            break;

            case '5':
                printf("Vase volba=%c\n", c);
                printf("Delete_First - vymaz  1.prvek");

                List_Delete_First(&seznam);
                print_seznam(&seznam);
            break;

            case '6':
                printf("Vase volba=%c\n", c);
                printf("Post_Delete - vymaz prvek za aktivnim prvkem\n");

                List_Post_Delete(&seznam);
                print_seznam(&seznam);
            break;

            case '7':
                printf("Vase volba=%c\n", c);
                printf("Post_Insert - vloz novy prvek za aktivni prvek");

                if (Data_Get(&data))
                    List_Post_Insert(&seznam, data);
                else
                    return EXIT_FAILURE;

                print_seznam(&seznam);
            break;

            case '8':
                printf("Vase volba=%c\n", c);
                printf("Copy - ziskani hodnoty aktivniho prvku \n");

                if (List_Copy(seznam, &data))
                    Data_Print(&data);

                print_seznam(&seznam);
            break;

            case '9':
                printf("Vase volba=%c\n", c);
                printf("Succ - posuv ukazatel aktivniho prvku na dalsi prvek\n");

                List_Succ(&seznam);
                print_seznam(&seznam);
            break;

            case 'A':
            case 'a':
                printf("Vase volba=%c\n", c);
                printf("Is_Active - zjisteni, zda je seznam aktivni\n");
                printf("Is_Active=");
                printf("%s\n", List_Is_Active(seznam) ? "true" : "false");
                print_seznam(&seznam);
            break;

            case 'M':
            case 'm':
                printf("Vase volba=%c\n", c);

                print_menu();

                break;


            default:
                run = false;
        }
    };


	return 0;
}

