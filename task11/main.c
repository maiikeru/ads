#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

FILE *file_in, *file_out;

enum {
    start = 0,
    lomitko = 1,
    note_line = 2,
    note_multi_line = 3,
    note_multi_line_end = 4,
    uvozovka = 5,
    uvozovka_lomitko = 6,
    apostrof = 7,
    apostrof_lomitko = 8
} stav;

void release_file(bool in, bool out){
    if (in) fclose(file_in);
    if (out) fclose(file_out);
}

int main( int argc, char** argv ) {

	(void) argc; // unsed
    (void) argv; // unsed

    bool flag_in = false, flag_out = false;
    // nezadan vstup
    if (argc == 1){
        // nastaven vstup
        file_in = stdin;

        // nastaven vystup
        file_out = stdout;
    }
    // nastaven vstup jako soubor a vystup na stdout
    else if (argc == 2){
        if ((file_in = fopen(argv[1], "r+")) == NULL){
            fputs("main: unable open input\n", stderr);
            return EXIT_FAILURE;
        }
        flag_in = true;
        file_out = stdout;
    }
    // vstup a vystup do souboru
    else if (argc == 3){
        if ((file_in = fopen(argv[1], "r+")) == NULL){
            fputs("main: unable open input\n", stderr);
            return EXIT_FAILURE;
        }
        flag_in = true;

        if ((file_out = fopen(argv[2], "w+")) == NULL){
            fputs("main: unable open output\n", stderr);
            fclose(file_in);
            return EXIT_FAILURE;
        }
        flag_out = true;
    }
    else{
       fputs("main: Invalit input\n", stderr);
        return EXIT_FAILURE;
    }


    int c;

    while ((c = fgetc(file_in)) != EOF){
        switch(stav){
            case start:
                if(c == '/') stav = lomitko;
                else if (c == '"') {
                    stav = uvozovka;
                    fputc(c, file_out);
                }
                else if (c == '\''){
                    stav = apostrof;
                    fputc(c, file_out);
                    }

                else
                    fputc(c, file_out);

            break;

            // je to poznamka?
            case lomitko:
                // /*
                if (c == '*') stav = note_multi_line;

                // //
                else if (c == '/') stav = note_line;

                // / -deleni, neni poznamka, navrat na zacatek
                else{
                    fputc('/', file_out);
                    fputc(c, file_out);
                    stav = start;
                }
            break;

            // radkova poznamka
            case note_line:
                if ((c == '\n') || (c == '\r') ){
                    fputc(c, file_out);
                    stav = start;
                }
            break;

            // vice radkova poznamka s *
            case note_multi_line:
                if (c == '*')
                    stav = note_multi_line_end;
            break;
            // konec vice radkovho komentare
            case note_multi_line_end:
                if(c == '/') stav = start;
                else stav = note_multi_line;
            break;

            case uvozovka:
                if (c == '"')
                    stav = start;
                else if (c == '\\')
                    stav = uvozovka_lomitko;

                fputc(c, file_out);
            break;

            case uvozovka_lomitko:
                stav = uvozovka;
                fputc(c, file_out);
            break;

            case apostrof:
                if (c == '\'') stav = start;
                else if (c == '\\') stav = apostrof_lomitko;

                fputc(c, file_out);
            break;

            case apostrof_lomitko:
                fputc(c, file_out);
                stav = apostrof;
            break;
        }

    }

    release_file(flag_in, flag_out);

	return 0;
}

