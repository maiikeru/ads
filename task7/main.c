#include <stdio.h>
#include "vector.h"
#include "ioutils.h"
#include "histogram.h"
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

int main( void )
{
    long box_num = 0;
    printf( "Histogram\n" );
    Vector_t *vector = Vector_Create( 10, 10 );

    if( vector == NULL ) {
        printf( "Nepodarilo se alokovat pamet pro vektor." );
        return 0;
    }

    setvbuf( stdout, NULL, _IONBF, 0 );
    setvbuf( stderr, NULL, _IONBF, 0 );
    bool run = true;

    while( run ) {
        printf( "Stisknete:\n"
                "1 pro nacteni noveho souboru\n"
                "2 pro vypis histogramu\n"
                "3 pro vymazani obsahu vektoru\n"
                "cokoli jineho pro konec.\n" );
        char c;

        if( !io_utils_get_char( &c ) ) {
            break;
        }

        switch( c ) {
            case '1':
                printf( "Vlozte absolutni nebo relativni cestu k souboru:\n" );
                char filename[0x100];

                if( !io_utils_get_string( filename, 0x100 ) ) {
                    run = false;
                    break;
                }

                filename[strlen( filename ) - 1] = 0;

                if( Histogram_AnalyzeFile( vector, filename ) != 0 ) {
                    printf( "Nacitani souboru selhalo...\n" );
                }
                /*
                else{

                    printf( "Nacitani souboru probehlo uspesne...\n" );

                    for (uint64_t i = 1; i < (vector->size - vector->free_cells); i++){
                        printf("\n cetnost %" PRIu64 " radku: %" PRIu64 " \n", i + 1, vector->items[i]);
                    }

                }
                */
                break;

            case '2':
                printf( "Zadejte pocet boxu histogramu:\n" );

                if( !io_utils_get_long( &box_num ) ) {
                    run = false;
                    break;
                }

                Histogram_Visualize( vector, ( uint8_t )box_num );
                break;

            case '3':
                Vector_Clear( vector );
                break;

            default:
                run = false;
        }
    };

    printf( "Vracim alokovanou pamet.\n" );

    Vector_Clear( vector );

    free( vector );

    printf( "Veskera alokovana pamet byla vracena.\n" );
}

