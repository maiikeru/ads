#include "vector.h"

#include <stdlib.h>
#include <stdio.h>

void error(char * message){
    fputs(message, stderr);
    //exit(EXIT_FAILURE);
}

Vector_t *Vector_Create(uint64_t initial_size, uint32_t alloc_step)
{
    (void)initial_size;
    (void)alloc_step;
    Vector_t * novy = malloc(sizeof(Vector_t));
    if (novy == NULL) {
        error("Vector create: arror alocating Vector_t");
        return NULL;
    }

    novy->items = malloc(initial_size * sizeof(uint64_t));
    if (novy->items == NULL){
        error("Vector create: error alocating the array");
        free(novy);
        return NULL;
    }

    novy->size = novy->free_cells = initial_size;
    novy->alloc_step = alloc_step;

    return novy;
}

Vector_t *Vector_Copy(const Vector_t *original)
{
    (void) original;
    Vector_t * novy = malloc(sizeof(Vector_t));
    if (novy == NULL) {
        error("Vector_Copy: arror alocating Vector_t");
        return NULL;
    }

    novy->items = malloc(original->size * sizeof(uint64_t));
    if (novy->items == NULL){
        error("Vector_Copy: error alocating the array");
        free(novy);
        novy = NULL;
        return NULL;
    }

    novy->alloc_step = original->alloc_step;
    novy->free_cells = original->free_cells;
    novy->size = original->size;

    for(uint64_t i = 0; i < novy->size - novy->free_cells; i++)
        novy->items[i] = original->items[i];

    return novy;
}

void Vector_Clear(Vector_t *vector)
{
    (void)vector;

    free(vector->items);
    vector->items = NULL;
    vector->free_cells = 0;
    vector->size = 0;
}

uint64_t Vector_Length(Vector_t *vector)
{
    (void)vector;

    if (vector == NULL) return UINT64_MAX;

    return (vector->size - vector->free_cells);
}

bool Vector_At(Vector_t *vector, uint64_t position, uint64_t *value)
{
    (void)vector;
    (void)position;
    (void)value;
    if (position >= (vector->size - vector->free_cells)) return false;

    *value = vector->items[position];
    return true;
}

bool Vector_Remove(Vector_t *vector, uint64_t position)
{
    (void)vector;
    (void)position;
    if (position >= (vector->size - vector->free_cells)) return false;

    for (uint64_t i = position; i < (vector->size - vector->free_cells -1); i++)
        vector->items[i] = vector->items[i+1];

    vector->free_cells++;
    return true;

}

void Vector_Append(Vector_t *vector, uint64_t value)
{
    (void)vector;
    (void)value;

    if (vector->free_cells == 0){
        uint64_t * uk = realloc(vector->items, sizeof(uint64_t)*(vector->size + vector->alloc_step));
        if (uk ==NULL) {
            error("Vector_Append: error allocating memmory");
            free(uk);
            return;
        }
        vector->items = uk;
        vector->size += vector->alloc_step;
        vector->free_cells = vector->alloc_step;
    }
    uint64_t index = vector->size - vector->free_cells;
    vector->items[index] = value;
    vector->free_cells--;
}

bool Vector_Contains(Vector_t *vector, uint64_t value)
{
    (void)vector;
    (void)value;
    for (uint64_t i = 0; i < (vector->size - vector->free_cells); i++){
        if (vector->items[i] == value) return true;
    }

    return false;
}

int64_t Vector_IndexOf(Vector_t *vector, uint64_t value, uint64_t from)
{
    (void)vector;
    (void)value;
    (void)from;
    for (uint64_t i = from; i < (vector->size - vector->free_cells); i++){
        if (vector->items[i] == value) return i;
    }

    return -1;
}

void Vector_Fill(Vector_t *vector, uint64_t value, uint64_t start_position, uint64_t end_position)
{
    (void)vector;
    (void)value;
    (void)start_position;
    (void)end_position;

    if (start_position > (vector->size - vector->free_cells)) return;
    if (end_position > (vector->size - vector->free_cells)) end_position = (vector->size - vector->free_cells);
    for (uint64_t i = start_position; i <= end_position; i++)
        vector->items[i] = value;
}
