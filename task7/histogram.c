#include "histogram.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int Histogram_AnalyzeFile(Vector_t *vector, char *filename)
{
    (void)vector;
    (void)filename;

    uint64_t counter_row = 0;
    int znak;
    FILE * file;

    printf("Opening.. %s\n", filename);

    if ((file = fopen(filename, "r")) == NULL) return -1;
    while((znak = getc(file)) != EOF) {

        if((znak == '\n') || (znak == '\r') ){
            if (counter_row != 0)
                Vector_Append(vector, counter_row-1);

            counter_row = 0;
        }
        counter_row++;
    }
    // EOF
    //Vector_Append(vector, counter_row-1);
    fclose(file);
    return 0;
}

void Histogram_Visualize(Vector_t *vector, uint8_t box_num)
{
    (void)vector;
    (void)box_num;

    uint64_t min = vector->size, max = 0;

    for (uint64_t i = 1; i < (vector->size - vector->free_cells); i++){
        if (min > vector->items[i]) min = vector->items[i];
        if (max < vector->items[i]) max = vector->items[i];
    }
    min++;
    printf("Min: %" PRIu64 "\n", min);
	printf("Max: %" PRIu64 "\n", max);
    max++;
	uint64_t *rows;
	if ((rows = (uint64_t*)calloc(max, sizeof(uint64_t))) == NULL ){
        fputs("Histogram_Visualize: unable alloc memory", stderr);
        return;
	}

	for (uint64_t j = 1; j < (vector->size - vector->free_cells); j++){
        //printf("radek %" PRIu64 " ma cetnost %" PRIu64 "\n", j, vector->items[j]);
        //if (vector->items[j] != 0)
        rows[vector->items[j]]++;
    }

/*
    for (int k = 0; k < max; k++){
        printf("for; radek %d ma cetnost %" PRIu64 "\n", k, rows[k]);
    }
*/
    printf ("Histogram....\n");
    printf ("[From]\t[To]\t[Count]\n");

    uint64_t step = ((max - min) / box_num) + 1;
    uint64_t move = 1, sum, k;
    while (move < max){
        sum = 0;
        for (k = move; k < (step + move); k++){
            if (k < max) {
                sum+=rows[k];
                //printf("Sum: %" PRIu64 ", k: %" PRIu64 ", hodnota: %" PRIu64 "\n", sum, k, rows[k]);
            }
        }
        printf(" %" PRIu64 "\t%" PRIu64 "\t%" PRIu64 " \n",(step + move) - (step ), k-1 ,sum );
        move += step;
    }

    free(rows);
}
