#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UNUSED(__ARG__) ((void)__ARG__)

int main(int argc, char** argv)
{
    //printf("Hello");

    UNUSED(argc);
    UNUSED(argv);
    int vek, vyska, vaha, ok;
    int weight = 0, height = 0, age =0, pocet = 0, minVek, maxVek;
    char retezec[256], jmeno[256], oldest[256], youngest[256];
    while(fgets(retezec, 256, stdin) != NULL) {
        ok = sscanf(retezec, "%[^;];%d;%d;%d", jmeno, &vek, &vyska, &vaha);
         //printf("err=%d, jmeno=%s, vek=%d, vyska=%d, vaha=%d\n", 5, jmeno, vek, vyska, vaha);
        if(ok == 4) {
            // pokud jsou vsechny 4 udaje OK, zpracujeme je zde.

            if (pocet == 0){
            minVek = maxVek = vek;
            strcpy(youngest, jmeno);
            strcpy(oldest, jmeno);
            }
            else {
              if (vek > maxVek){
                maxVek = vek;
                strcpy(oldest, jmeno);
              }
              if(vek < minVek){
              minVek = vek;
              strcpy(youngest, jmeno);
              }
            }

            age +=vek;
            height +=vyska;
            weight +=vaha;
            pocet++;

        }
    }

    printf("Prumerna vaha je: %d kg\n", weight/pocet);
    printf("Prumerna vyska je: %d cm\n", height/pocet);
    printf("Prumerny vek je: %d\n", age/pocet);
    printf("Nejmladsim je %s s vekem %d\n", youngest, minVek);
    printf("Nejstarsim je %s s vekem %d\n", oldest, maxVek);

    return 0;
}

