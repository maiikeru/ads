# Úkol 1 - Načítání CSV souboru #

Příklad na procvičení čtení ze souboru.

### Vzorový vstup ###

```
Aneta Langerova;30;160;65;
Lojza;1;80;11;
Lidumil Nejed;50;160;70;
Drahomir Vladyka;40;180;80;
```

Tzn. v každém řádku je jméno, věk, výška, váha.

Vytvořte program, který spočítá průměrnou hodnotu věku, výšky, váhy u všech lidí.

Najde a vypíše také jména a věk nejmladšího a nejstaršího člověka.

### Vzorový výstup ###

```
Prumerna vaha je: 56 kg 
Prumerna vyska je: 145 cm 
Prumerny vek je: 30 
Nejmladsim je Lojza s vekem 1 
Nejstarsim je Lidumil Nejed s vekem 50
```
