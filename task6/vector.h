/**
 * @file    vector.h
 * @author  Roman Dosek, Tomas Jurena
 * @date    2/2015
 * @brief   Headers of Vector data structure
 */

#ifndef __VECTOR_H
#define __VECTOR_H

#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

/* Vector structure */
typedef struct {

    uint64_t *items;        /**< Internal pointer to allocated memory */
    uint64_t size;          /**< Number of currently allocated cells */
    uint64_t free_cells;     /**< Number of unused cells inside Vector */
    uint32_t alloc_step;    /**< Number of cells allocated during expanding */
} Vector_t;


void error(char * message);


/* Public Vector API */
/**
 * @brief   Vytvoří vektor s počáteční velikostí speicifikovanou parametrem. Po návratu z funkce je majitelem ukazetele na strukturu vektoru uživatel nikoli knihovna.
 * @param[in]   initial_size    počáteční velikost Vectoru
 * @param[in]   alloc_step      Určuje, po jak velkých blocích se přialokovává paměť
 * @return  Vrací nově alokovanou strukturu popisující Vector jinak NULL pokud dojde k problémům.
 */
Vector_t *Vector_Create( uint64_t initial_size, uint32_t alloc_step );

/**
 * @brief   Vytvoří samostatnou kopii vektoru
 * @param[in]   original    Ukazatel na kopírovaný Vector
 * @return  Vrací strukturu popisující zkopírovaný Vector. Při chybách vrátí NULL
 */
Vector_t *Vector_Copy( const Vector_t * const original );

/**
 * @brief   Vymaže obsah vektoru, uvolní paměť a nastaví jeho velikost na 0. Prvek alloc_step zůstává nezměněn.
 * @param[in]   vector  Ukazatel na Vector
 */
void Vector_Clear( Vector_t * const vector );

/**
 * @brief   Vrací aktuální délku vektoru
 * @param[in]   vector  Ukazatel na Vector
 * @return  Aktuální délka vektoru nebo UINT64_MAX pokud je předán NULL. Aktuální délka je počet prvků, které jsou v poli uloženy nikoli počet alokovaných prvků.
 */
uint64_t Vector_Length( const Vector_t * const vector );

/**
 * @brief   Navrací hodnotu vektoru na vybrané pozici
 * @param[in]   vector      Ukazatel na Vector
 * @param[in]   position    Pozice v rámci Vectoru
 * @param[out]   value       Ukazatel pro navrácení hodnoty pokud byla nalezena
 * @return  Vrací true pokud je prvek ve vektoru jinak false
 */
bool Vector_At( const Vector_t * const vector, uint64_t position, uint64_t * const value );

/**
 * @brief   Odstraní prvek na zvolené pozici a posune data o prvek doleva.
 * @param[in]   vector      Ukazatel na Vector
 * @param[in]   position    Pozice v rámci Vectoru
 * @return  Vrací true pokud byla pozice uvnitř rozsahu jinak false
 */
bool Vector_Remove( Vector_t * const vector, uint64_t position );

/**
 * @brief   Přidá novou hodnotu na konec vektoru
 * @param[in]   vector  Ukazatel na Vector
 * @param[in]   value   Vkládaná hodnota
 */
void Vector_Append( Vector_t * const vector, uint64_t value );

/**
 * @brief   Funkce slouží k zjištění, jestli se někde ve vektoru nachází daná hodnota
 * @param[in]   vector  Ukazatel na Vector
 * @param[in]   value   Hledaná hodnota
 * @return  Vrací true pokud je hodnota nalezena, jinak false
 */
bool Vector_Contains( const Vector_t * const vector, uint64_t value );

/**
 * @brief   Hledá pozici prvku s danou hodnotou ve Vectoru
 * @param[in]   vector  Ukazatel na Vector
 * @param[in]   value   Hledaná hodnota
 * @param[in]   from    Pozice od které se začíná hledat
 * @return  Vrací pozici prvku pokud je nalezen, jinak -1
 */
int64_t Vector_IndexOf( const Vector_t * const vector, uint64_t value, uint64_t from );

/**
 * @brief   Vyplní část vektoru zadanou rozsahem nějakou hodnotou. Vektor je přepsán od začáteční pozice až po koncovou (včetně).
 * Pokud je koncová pozice za aktuální délkou vektoru, je vyplněna část od počáteční pozice po poslení prvek vektoru. Jestli je počáteční pozice za koncem vektoru,
 * není vykonáno nic.
 * #note    Bonusová funkce pro aktivní studenty
 * @param[in]   vector          Ukazatel na Vector
 * @param[in]   value           Nastavovaná hodnota
 * @param[in]   start_position  Počáteční pozice
 * @param[in]   end_position    Konečná pozice
 */
void Vector_Fill( const Vector_t * const vector, uint64_t value, uint64_t start_position, uint64_t end_position );

#endif //__VECTOR_H
