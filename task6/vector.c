/**
 * @file    vector.c
 * @author  Roman Dosek, Tomas Jurena
 * @date    2/2015
 * @brief   Headers of Vector data structure
 */

#include "vector.h"
#include "mymalloc.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

void error(char * message){
    fputs(message, stderr);
    //exit(EXIT_FAILURE);
}


/* Public Vector API */
/**
 * @brief   Vytvoří vektor s počáteční velikostí speicifikovanou parametrem. Po návratu z funkce je majitelem ukazetele na strukturu vektoru uživatel nikoli knihovna.
 * @param[in]   initial_size    počáteční velikost Vectoru
 * @param[in]   alloc_step      Určuje, po jak velkých blocích se přialokovává paměť
 * @return  Vrací nově alokovanou strukturu popisující Vector jinak NULL pokud dojde k problémům.
 */
Vector_t *Vector_Create( uint64_t initial_size, uint32_t alloc_step ){
    Vector_t * novy = myMalloc(sizeof(Vector_t));
    if (novy == NULL) {
        error("Vector create: arror alocating Vector_t");
        return NULL;
    }

    novy->items = myMalloc(initial_size * sizeof(uint64_t));
    if (novy->items == NULL){
        error("Vector create: error alocating the array");
        myFree(novy);
        return NULL;
    }

    novy->size = novy->free_cells = initial_size;
    novy->alloc_step = alloc_step;

    return novy;
}

/**
 * @brief   Vytvoří samostatnou kopii vektoru
 * @param[in]   original    Ukazatel na kopírovaný Vector
 * @return  Vrací strukturu popisující zkopírovaný Vector. Při chybách vrátí NULL
 */
Vector_t *Vector_Copy( const Vector_t * const original ){
    Vector_t * novy = myMalloc(sizeof(Vector_t));
    if (novy == NULL) {
        error("Vector_Copy: arror alocating Vector_t");
        return NULL;
    }

    novy->items = myMalloc(original->size * sizeof(uint64_t));
    if (novy->items == NULL){
        error("Vector_Copy: error alocating the array");
        myFree(novy);
        novy = NULL;
        return NULL;
    }

    novy->alloc_step = original->alloc_step;
    novy->free_cells = original->free_cells;
    novy->size = original->size;

    for(uint64_t i = 0; i < novy->size - novy->free_cells; i++)
        novy->items[i] = original->items[i];

    return novy;
}

/**
 * @brief   Vymaže obsah vektoru, uvolní paměť a nastaví jeho velikost na 0. Prvek alloc_step zůstává nezměněn.
 * @param[in]   vector  Ukazatel na Vector
 */
void Vector_Clear( Vector_t * const vector ){
    myFree(vector->items);
    vector->items = NULL;
    vector->free_cells = 0;
    vector->size = 0;
}

/**
 * @brief   Vrací aktuální délku vektoru
 * @param[in]   vector  Ukazatel na Vector
 * @return  Aktuální délka vektoru nebo UINT64_MAX pokud je předán NULL. Aktuální délka je počet prvků, které jsou v poli uloženy nikoli počet alokovaných prvků.
 */
uint64_t Vector_Length( const Vector_t * const vector ){
     if (vector == NULL) return UINT64_MAX;

    return (vector->size - vector->free_cells);
}

/**
 * @brief   Navrací hodnotu vektoru na vybrané pozici
 * @param[in]   vector      Ukazatel na Vector
 * @param[in]   position    Pozice v rámci Vectoru
 * @param[out]   value       Ukazatel pro navrácení hodnoty pokud byla nalezena
 * @return  Vrací true pokud je prvek ve vektoru jinak false
 */
bool Vector_At( const Vector_t * const vector, uint64_t position, uint64_t * const value ){
    if (position >= (vector->size - vector->free_cells)) return false;

    *value = vector->items[position];
    return true;
}

/**
 * @brief   Odstraní prvek na zvolené pozici a posune data o prvek doleva.
 * @param[in]   vector      Ukazatel na Vector
 * @param[in]   position    Pozice v rámci Vectoru
 * @return  Vrací true pokud byla pozice uvnitř rozsahu jinak false
 */
bool Vector_Remove( Vector_t * const vector, uint64_t position ){
    if (position >= (vector->size - vector->free_cells)) return false;

    for (uint64_t i = position; i < (vector->size - vector->free_cells -1); i++)
        vector->items[i] = vector->items[i+1];

    vector->free_cells++;
    return true;
}

/**
 * @brief   Přidá novou hodnotu na konec vektoru
 * @param[in]   vector  Ukazatel na Vector
 * @param[in]   value   Vkládaná hodnota
 */
void Vector_Append( Vector_t * const vector, uint64_t value ){
    if (vector->free_cells == 0){
        uint64_t * uk = myRealloc(vector->items, sizeof(uint64_t)*(vector->size + vector->alloc_step));
        if (uk ==NULL) {
            error("Vector_Append: error allocating memmory");
            myFree(uk);
            return;
        }
        vector->items = uk;
        vector->size += vector->alloc_step;
        vector->free_cells = vector->alloc_step;
    }
    uint64_t index = vector->size - vector->free_cells;
    vector->items[index] = value;
    vector->free_cells--;
}

/**
 * @brief   Funkce slouží k zjištění, jestli se někde ve vektoru nachází daná hodnota
 * @param[in]   vector  Ukazatel na Vector
 * @param[in]   value   Hledaná hodnota
 * @return  Vrací true pokud je hodnota nalezena, jinak false
 */
bool Vector_Contains( const Vector_t * const vector, uint64_t value ){
    for (uint64_t i = 0; i < (vector->size - vector->free_cells); i++){
        if (vector->items[i] == value) return true;
    }
    return false;
}

/**
 * @brief   Hledá pozici prvku s danou hodnotou ve Vectoru
 * @param[in]   vector  Ukazatel na Vector
 * @param[in]   value   Hledaná hodnota
 * @param[in]   from    Pozice od které se začíná hledat
 * @return  Vrací pozici prvku pokud je nalezen, jinak -1
 */
int64_t Vector_IndexOf( const Vector_t * const vector, uint64_t value, uint64_t from ){
    for (uint64_t i = from; i < (vector->size - vector->free_cells); i++){
        if (vector->items[i] == value) return i;
    }
    return -1;
}

/**
 * @brief   Vyplní část vektoru zadanou rozsahem nějakou hodnotou. Vektor je přepsán od začáteční pozice až po koncovou (včetně).
 * Pokud je koncová pozice za aktuální délkou vektoru, je vyplněna část od počáteční pozice po poslení prvek vektoru. Jestli je počáteční pozice za koncem vektoru,
 * není vykonáno nic.
 * #note    Bonusová funkce pro aktivní studenty
 * @param[in]   vector          Ukazatel na Vector
 * @param[in]   value           Nastavovaná hodnota
 * @param[in]   start_position  Počáteční pozice
 * @param[in]   end_position    Konečná pozice
 */
void Vector_Fill( const Vector_t * const vector, uint64_t value, uint64_t start_position, uint64_t end_position ){
    if (start_position > (vector->size - vector->free_cells)) return;
    if (end_position > (vector->size - vector->free_cells)) end_position = (vector->size - vector->free_cells);
    for (uint64_t i = start_position; i <= end_position; i++)
        vector->items[i] = value;
}

