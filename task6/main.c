
#include <stdio.h>
#include "vector.h"
#include "ioutils.h"
#include <inttypes.h>
#include <stdlib.h>
#include "mymalloc.h"

void Release_memory(Vector_t *v){
    printf("Uvolnuji vsechnu pridelenou pamet.\n");
	myFree(v->items);
	myFree(v);
	printf("Vsechna pridelena pamet byla uvolnena.\n");
	//exit(EXIT_FAILURE);
}

int main(void)
{
    uint64_t size_vect, prirust;

    printf("Vector test program\n");
    printf("Zadejte prosim vychozi velikost pole a velikost prirustku.\nVelikost:\n");

    if(!io_utils_get_uint64_t(&size_vect)){
        printf("Konec souboru, koncime.\n");
        return EXIT_FAILURE;
    }

    printf("Prirustek\n");
    if(!io_utils_get_uint64_t(&prirust)){
        printf("Konec souboru, koncime.\n");
        return EXIT_FAILURE;
    }

    Vector_t *v = Vector_Create(size_vect, prirust);
    if (v == NULL){
        printf("Konec souboru, koncime.\n");
        return EXIT_FAILURE;
    }

    // napln2ni vektoru
    for (unsigned int i = 0; i < 100; i = i + 5){
		Vector_Append(v, i);
	}

    uint64_t value, startpos, endpos;
    Vector_t *copy;
    bool run = true;

    while( run ) {
        printf( "Stisknete:\n"
                "1 pro vypis delky vektoru\n"
                "2 pro pridani prvku\n"
                "3 pro odebrani prvku\n"
                "4 pro vypis vsech prvku\n"
                "5 pro zjisteni jestli vektor obsahuje prvek\n"
                "6 pro nalezeni pozice prvku\n"
                "7 pro vycisteni vektoru\n"
                "8 pro vyplneni casti vektoru predanou hodnotou\n"
                "9 pro vytvoreni kopie vektoru\n"
                "0 pro vypsani jednoho prvku\n"
                "cokoli jineho pro konec.\n");
        char c;

        if( !io_utils_get_char( &c ) ) {
            break;
        }

        switch( c ) {
            // "1 pro vypis delky vektoru\n"
            case '1':
                printf("Delka vektoru: %" PRIu64 "\n", Vector_Length(v));
            break;

            // "2 pro pridani prvku\n"
            case '2':
                printf( "Vlozte hodnotu prvku:\n" );
                if(!io_utils_get_uint64_t(&value)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }
                Vector_Append(v, value);
            break;

            // "3 pro odebrani prvku\n"
            case '3':
                printf("Zadejte pozici prvku ktery chcete odebrat:\n");
                if(!io_utils_get_uint64_t(&value)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }
                Vector_Remove(v, value);
            break;

            //"4 pro vypis vsech prvku\n"
            case '4':
                for (unsigned int i = 0; i < Vector_Length(v); i++){
                    Vector_At(v, i, &value);
                    printf("vector[%d]: %" PRIu64 "\n", i, value);
                }
            break;

            // "5 pro zjisteni jestli vektor obsahuje prvek\n"
            case '5':
                printf("Zadejte hodnotu hledaneho prvku:\n");
                if(!io_utils_get_uint64_t(&value)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }
                Vector_Contains(v , value) ? printf("Vector obsahuje prvek\n") : printf("Vector neobsahuje prvek\n");
            break;

            // "6 pro nalezeni pozice prvku\n"
            case '6':
                printf("Zadejte hodnotu hledaneho prvku:\n");
                if(!io_utils_get_uint64_t(&value)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }

                printf("Zadejte pocatecni index hledani:\n");
                if(!io_utils_get_uint64_t(&startpos)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }

                printf("Pozice prvku: %" PRId64 "\n", Vector_IndexOf(v, value, startpos));
            break;

            // "7 pro vycisteni vektoru\n"
            case '7':
                Vector_Clear(v);
            break;

            // "8 pro vyplneni casti vektoru predanou hodnotou\n"
            case '8':
                printf("Zadejte hodnotu, ktera ma byt zapsana:\n");
                if(!io_utils_get_uint64_t(&value)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }

                printf("Zadejte startovni pozici:\n");
                if(!io_utils_get_uint64_t(&startpos)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }

                printf("Zadejte koncovou pozici:\n");
                if(!io_utils_get_uint64_t(&endpos)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }

                Vector_Fill(v, value, startpos, endpos);
            break;

            // "9 pro vytvoreni kopie vektoru\n"
            case '9':
                copy = Vector_Copy(v);
                Vector_At(copy, 0, &value);
                printf("copy[0]: %" PRIu64 "\n", value);
				printf("Odebiram vektor \"copy\"\n");

				myFree(copy->items);
				myFree(copy);
				copy = NULL;
            break;

            //"0 pro vypsani jednoho prvku\n"
            case '0':
                printf("Zadejte index prvku:\n");
                if(!io_utils_get_uint64_t(&startpos)){
                    printf("Konec souboru, koncime.\n");
                    Release_memory(v);
                    return EXIT_FAILURE;
                }

                if(Vector_At(v, startpos, &value))
                    printf("vector[%" PRIu64 "]: %" PRIu64 "\n", startpos, value);
                else
                    printf("Prvek na indexu %" PRIu64 " nebyl ve vektoru nalezen.\n", startpos);

            break;

            default:
                run = false;
                Release_memory(v);
        }
    };





/*
    for (int i=0; i<1000; i++){
        Vector_Append(v, i);

        uint64_t val;
        Vector_At(v,i, &val);
        printf("%" PRIu64 ", ", val);
    }
*/



    return 0;
}
