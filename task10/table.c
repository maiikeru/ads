#include "table.h"
#include <stdlib.h>
#include "mymalloc.h"

void HashTable_Init(HashTable *table, size_t size, bool takeOwnership)
{
    (void)table;
    (void)size;
    (void)takeOwnership;

    //calloc vsechno nastavi na 0
    table->buckets = myMalloc(size * sizeof(table->buckets[0]));
    table->count = 0;
    table->size = size;
    table->take_ownership = takeOwnership;
}

void HashTable_Destruct(HashTable *table)
{
    (void)table;

    myFree(table->buckets);
    table->buckets = NULL;
    table->size = 0;
    table->take_ownership = false;

}

bool HashTable_Insert(HashTable *table, Data_t *key, Data_t *value)
{
    (void)table;
    (void)key;
    (void)value;

    unsigned index = Data_Hash(key);

    HashTableNode * uk  = table->buckets[index];

    while (uk != NULL){
        if (Data_Cmp(uk->key, key)  == 0) return false;

        uk = uk->next;
    }

    // vlozime novy prvek do seznamu jako  v operaci "insert frist"
    HashTableNode * novy = myMalloc(sizeof(HashTableNode));
    novy->key = key;
    novy->value= value;
    novy->next = table->buckets[index];
    table->buckets[index] = novy;
    table->count++;

    return true;
}

bool HashTable_Replace(HashTable *table, Data_t *key, Data_t *value)
{
    (void)table;
    (void)key;
    (void)value;

    unsigned index = Data_Hash(key);
    if(table->buckets[index] != NULL){
        table->buckets[index]->value = value;
        /*
        if (table->take_ownership){
            myFree(table->buckets[index]->key);
            myFree(table->buckets[index]->value);
        }
        */
        return true;
    }
    return false;
}

bool HashTable_Delete(HashTable *table, Data_t *key)
{
    (void)table;
    (void)key;

    unsigned index = Data_Hash(key);
    if(table->buckets[index] != NULL){
        myFree(table->buckets[index]->key);
        myFree(table->buckets[index]->value);
        return true;
    }
    return false;
}

Data_t *HashTable_Find(HashTable *table, Data_t *key)
{
    (void)table;
    (void)key;
    unsigned index = Data_Hash(key);
    if(table->buckets[index] != NULL)
        return table->buckets[index]->value;

    return NULL;
}

size_t HashTable_Get_Count(HashTable *table)
{
    (void)table;
    size_t cnt = 0;
    for (size_t i = 0; i < table->size; i++){
        if(table->buckets[i] != NULL)
            cnt++;
    }
    return cnt;
}

void HashTable_Clear(HashTable *table)
{
    (void)table;

    for (size_t i = 0; i < table->size; i++){
        if(table->buckets[i] != NULL) {
            HashTable_Delete(table,table->buckets[i]->key);
            myFree(table->buckets[i]);
        }
    }
}

void HashTable_Process(HashTable *table, TableNodeProc proc)
{
    (void)table;
    (void)proc;

    for (size_t i = 0; i < table->size; i++){
        if(table->buckets[i] != NULL)
            proc(table->buckets[i]->key, table->buckets[i]->value);
    }
}
