# HashTable #
Implementuje funkce pro hash tabulku.

```
Hash tabulka
----------

1 - HashTable_Init()
2 - HashTable_Destruct()
3 - HashTable_Insert()
4 - HashTable_Delete()
5 - HashTable_Find()
6 - HashTable_Get_Count()
7 - HashTable_Clear()
8 - HashTable_Process()
M - zobraz toto menu
K - konec
Pro ukonceni stiskni CTRL+D (Linux) nebo CTRL+Z (Windows).

Vase volba:
1
1024
3
123
Pepa
13
40
150
3
123
Lada
14
45
155
3
777123456789
Jan Zahradil
60
55
180
3
604111222333
Josef Slany
30
80
175
3
576035111
Alena Vratna
55
68
150
Zadej velikost tabulky (napr. 1000):
myMalloc: prideluji 4096 bajtu, celkove prideleno 4096 bajtu
Tabulka byla inicializovana.

Vase volba:
Zadejte klic:
myMalloc: prideluji 260 bajtu, celkove prideleno 4356 bajtu
Zadejte asociovanou hodnotu:
Jmeno: 
Zadejte vek: 
Zadejte vahu: 
Zadejte vysku: myMalloc: prideluji 288 bajtu, celkove prideleno 4644 bajtu
hash: Key: 123
 -> Hash index: 150
myMalloc: prideluji 12 bajtu, celkove prideleno 4656 bajtu

Vase volba:
Zadejte klic:
myMalloc: prideluji 260 bajtu, celkove prideleno 4916 bajtu
Zadejte asociovanou hodnotu:
Jmeno: 
Zadejte vek: 
Zadejte vahu: 
Zadejte vysku: myMalloc: prideluji 288 bajtu, celkove prideleno 5204 bajtu
Operace vlozeni se nezdarila (nezadali jste duplikatni klic?).
myFree: uvolnuji 260 bajtu, celkove prideleno 4944 bajtu
myFree: uvolnuji 288 bajtu, celkove prideleno 4656 bajtu

Vase volba:
Zadejte klic:
myMalloc: prideluji 260 bajtu, celkove prideleno 4916 bajtu
Zadejte asociovanou hodnotu:
Jmeno: 
Zadejte vek: 
Zadejte vahu: 
Zadejte vysku: myMalloc: prideluji 288 bajtu, celkove prideleno 5204 bajtu
hash: Key: 777123456789
 -> Hash index: 642
myMalloc: prideluji 12 bajtu, celkove prideleno 5216 bajtu

Vase volba:
Zadejte klic:
myMalloc: prideluji 260 bajtu, celkove prideleno 5476 bajtu
Zadejte asociovanou hodnotu:
Jmeno: 
Zadejte vek: 
Zadejte vahu: 
Zadejte vysku: myMalloc: prideluji 288 bajtu, celkove prideleno 5764 bajtu
hash: Key: 604111222333
 -> Hash index: 604
myMalloc: prideluji 12 bajtu, celkove prideleno 5776 bajtu

Vase volba:
Zadejte klic:
myMalloc: prideluji 260 bajtu, celkove prideleno 6036 bajtu
Zadejte asociovanou hodnotu:
Jmeno: 
Zadejte vek: 
Zadejte vahu: 
Zadejte vysku: myMalloc: prideluji 288 bajtu, celkove prideleno 6324 bajtu
hash: Key: 576035111
 -> Hash index: 461
myMalloc: prideluji 12 bajtu, celkove prideleno 6336 bajtu

Vase volba:
8
Klic: 123
 -> Hodnota: Pepa, 13.0, 40.0, 150.0
Klic: 576035111
 -> Hodnota: Alena Vratna, 55.0, 68.0, 150.0
Klic: 604111222333
 -> Hodnota: Josef Slany, 30.0, 80.0, 175.0
Klic: 777123456789
 -> Hodnota: Jan Zahradil, 60.0, 55.0, 180.0

Vase volba:
7
myFree: uvolnuji 260 bajtu, celkove prideleno 6076 bajtu
myFree: uvolnuji 288 bajtu, celkove prideleno 5788 bajtu
myFree: uvolnuji 12 bajtu, celkove prideleno 5776 bajtu
myFree: uvolnuji 260 bajtu, celkove prideleno 5516 bajtu
myFree: uvolnuji 288 bajtu, celkove prideleno 5228 bajtu
myFree: uvolnuji 12 bajtu, celkove prideleno 5216 bajtu
myFree: uvolnuji 260 bajtu, celkove prideleno 4956 bajtu
myFree: uvolnuji 288 bajtu, celkove prideleno 4668 bajtu
myFree: uvolnuji 12 bajtu, celkove prideleno 4656 bajtu
myFree: uvolnuji 260 bajtu, celkove prideleno 4396 bajtu
myFree: uvolnuji 288 bajtu, celkove prideleno 4108 bajtu
myFree: uvolnuji 12 bajtu, celkove prideleno 4096 bajtu
Vsechny polozky tabulky byly odstraneny.

Vase volba:
2
myFree: uvolnuji 4096 bajtu, celkove prideleno 0 bajtu
Tabulka byla destruovana.

Vase volba:
K

Konec.
```
