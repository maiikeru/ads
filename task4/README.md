# Úkol 4 - Telefonní seznam #
Vytvořte program "telefonní seznam", který bude pracovat se záznamy typu:

```
typedef struct {
  int id;
  char jmeno[64], prijmeni[64], adresa[256], telefon[32];
} tClovek;
```

Program bude načítat data ze vstupního CSV souboru (viz minulý příklad) do pole s dostatečnou velikostí. Název souboru bude předávám jako argument programu.

Pro seřazení dat použijte funkci qsort.

Příklad spuštění programu
```
./task4 -type i -file addressbook.csv
```

Příklad výstupu pro příkaz `./task4 -type p -file addressBook.csv`
```
3; Petr; Dostál; Uherské Hradiště 988; 874526548
4; Tomáš; Jarní; Zlín 2154; 695485265
1; Jan; Novák; Zlín 1234; 695842256
2; Michal; Nový; Otrokovice 12; 465851235
```

Příklad výstupu pro příkaz `./task4 -type i -file addressBook.csv`
```
1; Jan; Novák; Zlín 1234; 695842256
2; Michal; Nový; Otrokovice 12; 465851235
3; Petr; Dostál; Uherské Hradiště 988; 874526548
4; Tomáš; Jarní; Zlín 2154; 695485265
```

Příklad výstupu pro příkaz `./task4 -type a -file addressBook.csv`
```
2; Michal; Nový; Otrokovice 12; 465851235
3; Petr; Dostál; Uherské Hradiště 988; 874526548
1; Jan; Novák; Zlín 1234; 695842256
4; Tomáš; Jarní; Zlín 2154; 695485265
```
