#include "fce.h"

void minimax(int *pole, int delka, int *minimum, int *maximum)
{
    (void)pole;
    (void)delka;
    (void)minimum;
    (void)maximum;

    *minimum = getMin(pole, delka);
    *maximum = getMax(pole, delka);
}

int getMin(int *pole, int delka)
{
    (void)pole;
    (void)delka;

    int min = pole[0];
    for (int i = 0; i < delka; i++)
        if (pole[i] < min) min = pole[i];

    return min;
}

int getMax(int *pole, int delka)
{
    (void)pole;
    (void)delka;

    int max = pole[0];
    for (int i = 0; i < delka; i++)
        if (pole[i] > max) max = pole[i];

    return max;
}

double fact(double n)
{
    (void)n;

    if(n < 0) return 0;

    if((n ==0) || (n == 1)) return 1;

    return n * fact(n -1);
}

int fibonacci(int n)
{
    (void)n;

    if(n <= 1) return n;

    return fibonacci(n -1) + fibonacci(n -2);
}
