# Knihovna matematických funkcí #
Vytvořte implementaci knihovny matematických funkcí dle vzorové ukázky:

Ukázkový běh programu:
```
#~/MathLib$ ./a.out 
Stisknete:
1 pro minmax
2 pro minimum
3 pro maximum
4 pro faktorial
5 pro Fibonacciho posloupnost
cokoli jineho pro konec.
1
Aktualni obsah pole:
1, 2, 3, 4, 5, 6, 7, 8, 9, 10
Zadat jine hodnoty? (A/N)
N
Minimum=1, maximum=10
Stisknete:
1 pro minmax
2 pro minimum
3 pro maximum
4 pro faktorial
5 pro Fibonacciho posloupnost
cokoli jineho pro konec.
2
Aktualni obsah pole:
1, 2, 3, 4, 5, 6, 7, 8, 9, 10
Zadat jine hodnoty? (A/N)
N
Minimum=1
Stisknete:
1 pro minmax
2 pro minimum
3 pro maximum
4 pro faktorial
5 pro Fibonacciho posloupnost
cokoli jineho pro konec.
3
Aktualni obsah pole:
1, 2, 3, 4, 5, 6, 7, 8, 9, 10
Zadat jine hodnoty? (A/N)
N
Maximum=10
Stisknete:
1 pro minmax
2 pro minimum
3 pro maximum
4 pro faktorial
5 pro Fibonacciho posloupnost
cokoli jineho pro konec.
4
Vlozte hodnotu N:
10
Faktorial(10)=3628800
Stisknete:
1 pro minmax
2 pro minimum
3 pro maximum
4 pro faktorial
5 pro Fibonacciho posloupnost
cokoli jineho pro konec.
5
Vlozte hodnotu N:
10
Fibonacci(10)=55
Stisknete:
1 pro minmax
2 pro minimum
3 pro maximum
4 pro faktorial
5 pro Fibonacciho posloupnost
cokoli jineho pro konec.
K
```
