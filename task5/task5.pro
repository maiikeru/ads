TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS += -Wall -Werror -std=c99

SOURCES += \
        main.c \
        ioutils.c \
        fce.c

HEADERS += \
        fce.h \
        ioutils.h

DESTDIR = bin
