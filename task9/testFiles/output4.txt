Strom v1.0
----------

1 - tree_init()
2 - tree_clear()
3 - tree_insert()
4 - tree_delete()
5 - tree_find_node()
6 - tree_get_node_count()
7 - tree_proces()
M - zobraz toto menu
K - konec
Pro ukonceni stiskni CTRL+D (Linux) nebo CTRL+Z (Windows).

Vase volba: 1
Strom byl inicializovan.

Vase volba: 7
Pruchod:
1 - preorder
2 - inorder
3 - postorder

Vase volba: 1


Vase volba: 3
Data uzlu stromu:
Zadejte jmeno osoby: Zadejte vek, vahu a vysku (oddelene Enter):
myMalloc: prideluji 296 bajtu, celkove prideleno 296 bajtu

Vase volba: 3
Data uzlu stromu:
Zadejte jmeno osoby: Zadejte vek, vahu a vysku (oddelene Enter):
myMalloc: prideluji 296 bajtu, celkove prideleno 592 bajtu

Vase volba: 3
Data uzlu stromu:
Zadejte jmeno osoby: Zadejte vek, vahu a vysku (oddelene Enter):
myMalloc: prideluji 296 bajtu, celkove prideleno 888 bajtu

Vase volba: 3
Data uzlu stromu:
Zadejte jmeno osoby: Zadejte vek, vahu a vysku (oddelene Enter):
myMalloc: prideluji 296 bajtu, celkove prideleno 1184 bajtu

Vase volba: 3
Data uzlu stromu:
Zadejte jmeno osoby: Zadejte vek, vahu a vysku (oddelene Enter):
myMalloc: prideluji 296 bajtu, celkove prideleno 1480 bajtu

Vase volba: 7
Pruchod:
1 - preorder
2 - inorder
3 - postorder

Vase volba: 1

Jmeno=Franta, vek=50.0, vaha=80.0, vyska=180.0
 | L -> Jmeno=Anna, vek=30.0, vaha=60.0, vyska=160.0
 | R -> Jmeno=Xaver, vek=60.0, vaha=70.0, vyska=175.0

Jmeno=Anna, vek=30.0, vaha=60.0, vyska=160.0
 | L -> NULL | R -> NULL
Jmeno=Xaver, vek=60.0, vaha=70.0, vyska=175.0
 | L -> Jmeno=Petr, vek=50.0, vaha=80.0, vyska=150.0
 | R -> NULL
Jmeno=Petr, vek=50.0, vaha=80.0, vyska=150.0
 | L -> Jmeno=Michal, vek=75.0, vaha=50.0, vyska=175.0
 | R -> NULL
Jmeno=Michal, vek=75.0, vaha=50.0, vyska=175.0
 | L -> NULL | R -> NULL

Vase volba: 7
Pruchod:
1 - preorder
2 - inorder
3 - postorder

Vase volba: 3

Jmeno=Anna, vek=30.0, vaha=60.0, vyska=160.0
 | L -> NULL | R -> NULL
Jmeno=Michal, vek=75.0, vaha=50.0, vyska=175.0
 | L -> NULL | R -> NULL
Jmeno=Petr, vek=50.0, vaha=80.0, vyska=150.0
 | L -> Jmeno=Michal, vek=75.0, vaha=50.0, vyska=175.0
 | R -> NULL
Jmeno=Xaver, vek=60.0, vaha=70.0, vyska=175.0
 | L -> Jmeno=Petr, vek=50.0, vaha=80.0, vyska=150.0
 | R -> NULL
Jmeno=Franta, vek=50.0, vaha=80.0, vyska=180.0
 | L -> Jmeno=Anna, vek=30.0, vaha=60.0, vyska=160.0
 | R -> Jmeno=Xaver, vek=60.0, vaha=70.0, vyska=175.0


Vase volba: 7
Pruchod:
1 - preorder
2 - inorder
3 - postorder

Vase volba: 2

Jmeno=Anna, vek=30.0, vaha=60.0, vyska=160.0
 | L -> NULL | R -> NULL
Jmeno=Franta, vek=50.0, vaha=80.0, vyska=180.0
 | L -> Jmeno=Anna, vek=30.0, vaha=60.0, vyska=160.0
 | R -> Jmeno=Xaver, vek=60.0, vaha=70.0, vyska=175.0

Jmeno=Michal, vek=75.0, vaha=50.0, vyska=175.0
 | L -> NULL | R -> NULL
Jmeno=Petr, vek=50.0, vaha=80.0, vyska=150.0
 | L -> Jmeno=Michal, vek=75.0, vaha=50.0, vyska=175.0
 | R -> NULL
Jmeno=Xaver, vek=60.0, vaha=70.0, vyska=175.0
 | L -> Jmeno=Petr, vek=50.0, vaha=80.0, vyska=150.0
 | R -> NULL

Vase volba: k
Konec.

myFree: uvolnuji 296 bajtu, celkove prideleno 1184 bajtu
myFree: uvolnuji 296 bajtu, celkove prideleno 888 bajtu
myFree: uvolnuji 296 bajtu, celkove prideleno 592 bajtu
myFree: uvolnuji 296 bajtu, celkove prideleno 296 bajtu
myFree: uvolnuji 296 bajtu, celkove prideleno 0 bajtu
