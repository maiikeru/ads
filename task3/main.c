// Private includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

// Private defines
#define VELIKOST (8000000u)
#define BUFFER_SIZE (256)

// const int VELIKOST 100000;
// Private function prototypes
int handleCommandLineArguments( int argc, char** argv, unsigned int *seed, char *path );

// Private functions


int find_char( int pocet);


int main( int argc, char** argv )
{
    static char* pole[VELIKOST]; // static umožní větší velikost, je uložený v paměti globálních proměnných
    unsigned int seed = 0;
    char filePath[0x100];

    if( handleCommandLineArguments( argc, argv, &seed, filePath ) == 0 ) {
        printf( "Nespravny pocet vstupnich argumentu nebo nespravny format argumentu." );
        return 0;
    }

    printf( "Hodnota seed: %d\n", seed );
    FILE * vstup = fopen( filePath, "r" );

    if( vstup == NULL ) {
        printf( "Soubor \"%s\" se nepodarilo otevrit", filePath );
        return 0;
    }

    srand( seed );
    char buffer[256];
    unsigned int i = 0;

    while( fscanf( vstup, "%255s", buffer ) != EOF && i < VELIKOST ) {
        unsigned long delka = strlen( buffer );

        if( delka > 1 ) {
            char* novy = malloc( delka + 1 );
            strcpy( novy, buffer );
            pole[i++] = novy;
        }

        /*
        // just for evaluation
        if( ( i & ( 4 * 65536 - 1 ) ) == 0 ) {
            printf( "Precten %d. radek\n", i );

        }
        */
    }
    // ukonceni souboru
    fclose(vstup);
    /*

    unsigned int N = i;

    // just for evaluation
    for( i = 0; i < N; i++ ) {
        puts( pole[i] );
    }*/


    char tajenka[BUFFER_SIZE];
    char words[BUFFER_SIZE][BUFFER_SIZE];
    int indexTajenky = rand() % i;
    puts( pole[indexTajenky] ); //debug
    strcpy(tajenka, pole[indexTajenky]);

    int  gen_index, pos;
    char * pch;
    pch = NULL;
    bool found = false;
    int center =0;
    for (unsigned int t = 0; t < strlen(tajenka); t++){
        while(!found){
            gen_index = rand() % i;
            strcpy(words[t], pole[gen_index]);
            //printf("Slovo: %s", words[t]);

            pch = (char*) memchr (words[t], tajenka[t], strlen(words[t]));
          if (pch!=NULL){
            //printf (" Slovo: '%s' a pismenko  found at position %d.\n", words[t], pch-words[t]+1);
            found = true;

            if ((pch-words[t]+1) > center) center =pch-words[t]+1;
          }

        }
        found = false;
        pch = NULL;
    }

    printf("Tajenka \"%s\"\n\n", tajenka);

    for (unsigned k = 0; k < strlen(tajenka); k++){

        pch = (char*) memchr (words[k], tajenka[k], strlen(words[k]));
        pos = pch-words[k];

        for (unsigned l = 1; l < center - pos; l++)printf(" ");

        for (unsigned m = 0; m <= strlen(words[k]); m++){
            if(m == pos || m == pos+1)
                printf("%c",'|');
            printf("%c",words[k][m]);
        }
        printf("\n");
        //printf("  :%d  :%d\n", pos, center);
    }
    //uvoleni pole
    for (unsigned n = 0; n < VELIKOST; n++)
        free(pole[n]);

    return 0;
}

/**
 * @brief Try to find seed in given commandline arguments
 * @param[in] argc Count of arguments
 * @param[in] argv Arguments
 * @param[out] seed Pointer to return seed
 * @return Returns 0 of argument count is not 1 or 3. Otherwise return 1;
 */
int handleCommandLineArguments( int argc, char** argv, unsigned int *seed, char *path )
{
    // parse seed and file path from cmd line
    if( argc == 5 ) {
        int correct = 0;

        for( int i = 1; i < 5; i += 2 ) {
            if( strcmp( argv[i], "-seed" ) == 0 ) {
                *seed = ( unsigned int )( strtol( argv[i + 1], NULL, 10 ) );

                if( *seed != 0 ) {
                    correct++;
                }

                continue;
            }

            if( strcmp( argv[i], "-file" ) == 0 ) {
                strcpy( path, argv[i + 1] );
                correct++;
            }
        }

        if( correct == 2 ) {
            return 1;
        }

        return 0;
    } else if( argc == 3 ) {
        // just path is passed to binary
        strcpy( path, argv[2] );
        *seed = ( unsigned int )( time( NULL ) );
        return 1;
    }

    return 0;
}
