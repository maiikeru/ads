# Úkol 3 - Generátor křížovek #

Vytvořte program, který do pole řetězců načte [wordlist](http://gpsfreemaps.net/navody/security/komplexni-cesky-a-slovensky-wordlist-ke-stazeni) ([v ZIPu ke stažení zde](http://vyuka.fai.utb.cz/pluginfile.php?file=/57350/mod_assign/intro/CZ.zip)), tj. soubor, obsahující slova nějakého jazyka. Následně pole použije k vygenerování křížovky takto:

z pole řetězců vyberte náhodný řetězec, který bude tajenkou berete z řetězce písmenko po písmenku a s každým písmenkem děláte toto:
z pole řetězců vyberete náhodný řetězec a podíváte se, zda obsahuje aktuální písmeno. Dokud ne, vezmete další řetězec (náhodně vybraný, nebo další v pořadí) 
pokud řetězec obsahuje písmenko, pak jej vytiskněte s odsazením o patřičný počet mezer, odpovídající pozici písmenka v řetězci. Písmeno tajenky bude ohraničeno v symbolu *pipe* (**|**).

Program musí pracovat se vstupními argumenty tak, že bude možné zadat seed hodnotu pro inicializaci generátoru náhodných čísel a cestu k souboru se slovy. Níže je příklad jak může být program spuštěn.
Po kontrole vstupních argumentů bude otevřen soubor v cestě předené vstupním argumentem a pracuje se s tímto souborem, nikoly se standardním vstupem.

Spuštění s pěti argumenty
```
./task3 -seed 152 -file ../../task3/CZ.txt
```

Spuštění se třemi argumenty
```
./task3 -file ../../task3/CZ.txt
```

Výstup může být obbdobný 
Program byl volán s následujícími parametry `./task3 -file ../../task3/CZ.txt -seed 453`:
```
Hodnota seed: 453
Tajenka "Wyndhamovým"

             |W|endovym
       karbox|y|lovými
předpovídatel|n|osti
             |d|vacky
      procmuc|h|as
           tm|a|vocernou
   Rajsiglový|m|a
        nevyp|o|dložili
  nejneskripa|v|ejsiho
     Sojnekov|ý|mi
         Kody|m|ove
```


Program byl volán s následujícími parametry `./task3 -seed 451 -file ../../task3/CZ.txt`:
```
Hodnota seed: 453
Tajenka "Wyndhamovým"

             |W|endovym
       karbox|y|lovými
předpovídatel|n|osti
             |d|vacky
      procmuc|h|as
           tm|a|vocernou
   Rajsiglový|m|a
        nevyp|o|dložili
  nejneskripa|v|ejsiho
     Sojnekov|ý|mi
         Kody|m|ove
```

Program byl volán s následujícími parametry `./task3 -file ../../task3/CZ.txt`:
```
Hodnota seed: 1521718831
Tajenka "svatorecenemu"

  nejzhor|s|enejsima
korporati|v|nich
     nevl|a|daří
      ces|t|nosti
      Leh|o|ckeho
    dekla|r|ovanýma
        n|e|doloupány
Skuderovy|c|h
        n|e|zažádati
         |n|evystvavani
        n|e|jsoupeřivějších
       ne|m|utovává
        H|u|bičkové
```

Program byl volán s následujícími parametry `./task3`:
```
Nespravny pocet vstupnich argumentu nebo nespravny format argumentu.
```
