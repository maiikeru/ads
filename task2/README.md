# Úkol 2 - Výpis kombinací 2 hráčů při tenisovém turnaji "každý s každým" #
Vytvořte program, který vypíše všechny unikátní kombinace 2 hráčů při tenisovém (šachovém, fotbalovém, karate...) turnaji, kde se má  utkat "každý s každým".
Uživatel zadá jména N hráčů (každé jméno na novém řádku), načež program vytiskne každou kombinaci na samostatný řádek.

### Příklad vstupu - pro turnaj se 4mi hráči ###
```
Jmeno1
Jmeno2
Jmeno3
Jmeno4
```

### Výstup ###
```
Jmeno1,Jmeno2
Jmeno1,Jmeno3
Jmeno1,Jmeno4
Jmeno2,Jmeno3
Jmeno2,Jmeno4
Jmeno3,Jmeno4
```
Pozn.: unikátní kombinace si můžete snadno graficky odvodit pomocí matice hráčů, ze které škrtnete diagonálu, protože hráči nehrají sami se sebou, a také celou polovinu pod (nebo nad) diagonálou, protože tyto poloviny jsou vzájemně symetrické (kombinace 1,3 je stejná jako 3,1).
